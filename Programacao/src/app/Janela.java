package app;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bd.Ingredientes;
import bd.Receitas;
import bd.Utilizador;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.ListModel;

import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.AbstractButton;
import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Janela extends JFrame {

	public static final String label = null;
	private JPanel contentPane;
	private JTextField nomeReceita;
	protected String selecionado;
	protected String ver;
	String passos;
	static int horas;
	static int minutos;
	static int segundos;
	static int milisegundos;
	
	static boolean state = true;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Janela frame = new Janela();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Janela() {
		setTitle("O MESTRE DAS RECEITAS");
		setExtendedState(MAXIMIZED_BOTH);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Janela.class.getResource("/imagens/logo3.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1426, 729);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(128, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		this.setLocationRelativeTo(null);
		
		JButton btnSair = new JButton("Sair");
		btnSair.setBackground(new Color(192, 192, 192));
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new Login().setVisible(true);	
				setVisible(false);
			}
		});
		btnSair.setBounds(1227, 11, 133, 52);
		contentPane.add(btnSair);
		
		JLabel lblReceitas = new JLabel("RECEITAS :");
		lblReceitas.setForeground(new Color(255, 255, 255));
		lblReceitas.setFont(new Font("Tahoma", Font.BOLD, 23));
		lblReceitas.setBounds(78, 11, 412, 52);
		contentPane.add(lblReceitas);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(78, 74, 294, 226);
		contentPane.add(scrollPane);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(901, 445, 393, 205);
		contentPane.add(scrollPane_1);
		
		JTextPane textPane = new JTextPane();
		textPane.setForeground(new Color(0, 0, 0));
		scrollPane_1.setViewportView(textPane);
		textPane.setBackground(new Color(128, 128, 128));
		
		JLabel lblDoses = new JLabel("DOSES:");
		lblDoses.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblDoses.setBounds(537, 30, 119, 33);
		contentPane.add(lblDoses);
		
		JLabel lblTempoDePreparao = new JLabel("TEMPO DE PREPARA\u00C7\u00C3O:");
		lblTempoDePreparao.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblTempoDePreparao.setBounds(666, 30, 267, 32);
		contentPane.add(lblTempoDePreparao);
		
		JLabel lblTempoDeConfeo = new JLabel("TEMPO DE CONFE\u00C7\u00C3O:");
		lblTempoDeConfeo.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblTempoDeConfeo.setBounds(950, 31, 267, 32);
		contentPane.add(lblTempoDeConfeo);
		
		JLabel label_foto = new JLabel("");
		label_foto.setIcon(null);
		label_foto.setBounds(78, 345, 780, 263);
		contentPane.add(label_foto);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(901, 179, 393, 226);
		contentPane.add(scrollPane_2);
		
		JList list_1 = new JList();
		list_1.setForeground(new Color(0, 0, 0));
		scrollPane_2.setViewportView(list_1);
		list_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		list_1.setBackground(new Color(128, 128, 128));
		
		JList list = new JList();
		list.addMouseListener(new MouseAdapter() {
			private int ingredientes;
			private String nomeIngredientes;
			private int dose;
			private String tempoP;
			private String tempoC;
			private String foto;
			private String tipo;
			

			private Connection connect() {
				 // SQLite connection string
		        String url = "jdbc:sqlite:test.db";
		        Connection conn = null;
		        try {
		            conn = DriverManager.getConnection(url);
		        } catch (SQLException e) {
		            System.out.println(e.getMessage());
		        }
		        return conn;
			}

			@Override
			public void mouseClicked(MouseEvent arg0) {
				selecionado = list.getSelectedValue().toString();
			
				dose = new Receitas().dose(selecionado); 
				tempoC = new Receitas().tempoC(selecionado);
				tempoP = new Receitas().tempoP(selecionado);
				foto = new  Receitas().foto(selecionado);
				tipo = new Receitas().tipo(selecionado);
				
				ver = new Receitas().verDescricao(selecionado);
				//System.out.println(ver);
				ingredientes = new Ingredientes().verId(selecionado);
				
				DefaultListModel DLM = new DefaultListModel();
				String sql = "SELECT * FROM DetalhesReceita WHERE idReceita = " +ingredientes;
				
				
				
				
				nomeIngredientes = new Ingredientes().verIngredientes(ingredientes);
		        try (Connection conn = this.connect();
		             Statement stmt  = conn.createStatement();
		             ResultSet rs    = stmt.executeQuery(sql)){
		        	
		        	 
		        	 // loop through the result set
		            while(rs.next()) {
		        
		            //	System.out.println(rs.getString("nome"));
		            	
						DLM.addElement(rs.getString("qtd")+" "+rs.getString("unidade")+"- "+rs.getString("nome"));
							
						}
						
						
		            	
		            
		        } catch (SQLException e) {
		            System.out.println(e.getMessage());
		        }
				new Receitas().listaReceitas();
				list_1.setModel(DLM);
				
				
				lblDoses.setText("DOSES: "+dose);
				lblTempoDeConfeo.setText("TEMPO DE CONFE��O: "+tempoC);
				lblTempoDePreparao.setText("TEMPO DE PREPARA��O: "+tempoP);
				ImageIcon icon=new ImageIcon(foto); 
				label_foto.setIcon(icon);
				lblReceitas.setText("RECEITAS : "+tipo);
				textPane.setText(ver);
				//list_1.setModel(DLM);
				System.out.println(nomeIngredientes);
				System.out.println(ingredientes);
				
				
			
			}
		});
		list.setForeground(new Color(0, 0, 0));
		list.setFont(new Font("Tahoma", Font.BOLD, 16));
		list.setBackground(new Color(128, 128, 128));
		scrollPane.setViewportView(list);
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		
		
		
		JButton btnAtualizar = new JButton("Mostrar");
		btnAtualizar.setBackground(new Color(192, 192, 192));
		btnAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				DefaultListModel DLM = new DefaultListModel();
//				DLM.addElement("ola");
//				DLM.addElement("boas");
				String sql = "SELECT nome FROM Receitas";
				
		        try (Connection conn = this.connect();
		             Statement stmt  = conn.createStatement();
		             ResultSet rs    = stmt.executeQuery(sql)){
		        	
		        	 
		        	 // loop through the result set
		            while(rs.next()) {
		        
		            //	System.out.println(rs.getString("nome"));
		            	
						DLM.addElement(rs.getString("nome"));
							
						}
						
						
		            	
		            
		        } catch (SQLException e) {
		            System.out.println(e.getMessage());
		        }
				new Receitas().listaReceitas();
				list.setModel(DLM);

			}
			private Connection connect() {
				 // SQLite connection string
		        String url = "jdbc:sqlite:test.db";
		        Connection conn = null;
		        try {
		            conn = DriverManager.getConnection(url);
		        } catch (SQLException e) {
		            System.out.println(e.getMessage());
		        }
		        return conn;
			}
			
		});
		btnAtualizar.setBounds(382, 76, 89, 23);
		contentPane.add(btnAtualizar);
		
		JButton btnApagar = new JButton("Apagar");
		btnApagar.setBackground(new Color(192, 192, 192));
		btnApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Receitas().apagarReceita(selecionado);
			}
		});
		btnApagar.setBounds(283, 311, 89, 23);
		contentPane.add(btnApagar);
		
		JButton btnNova = new JButton("Nova");
		btnNova.setBackground(new Color(192, 192, 192));
		btnNova.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			//	new Receitas().addReceita(nomeReceita.getText());
			//	nomeReceita.setText("");
				new CriarReceita().setVisible(true);
				setVisible(false);
	
			}
		});
		btnNova.setBounds(184, 311, 89, 23);
		contentPane.add(btnNova);
		
		nomeReceita = new JTextField();
		nomeReceita.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				String procura = nomeReceita.getText();
				DefaultListModel DLM = new DefaultListModel();

				String sql = "SELECT * FROM Receitas WHERE nome LIKE \"%"+procura+"%\" OR tipo LIKE \"%"+procura+"%\"";
				
		        try (Connection conn = this.connect();
		             Statement stmt  = conn.createStatement();
		             ResultSet rs    = stmt.executeQuery(sql)){
		        	
		        
		            while(rs.next()) {
		        
						DLM.addElement(rs.getString("nome"));
							
						}
						
						
		            	
		            
		        } catch (SQLException e) {
		            System.out.println(e.getMessage());
		        }
				new Receitas().listaReceitas();
				list.setModel(DLM);
				System.out.println(procura);
			
			}
			private Connection connect() {
				 // SQLite connection string
		        String url = "jdbc:sqlite:test.db";
		        Connection conn = null;
		        try {
		            conn = DriverManager.getConnection(url);
		        } catch (SQLException e) {
		            System.out.println(e.getMessage());
		        }
		        return conn;
			
			}
		});
		nomeReceita.setBounds(78, 311, 86, 20);
		contentPane.add(nomeReceita);
		nomeReceita.setColumns(10);
		
		JLabel lblPassos = new JLabel("Passos:");
		lblPassos.setForeground(new Color(255, 255, 255));
		lblPassos.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPassos.setBackground(new Color(218, 165, 32));
		lblPassos.setBounds(901, 416, 119, 32);
		contentPane.add(lblPassos);
		
		
		
		JLabel lblIngredientes = new JLabel("Ingredientes:");
		lblIngredientes.setForeground(new Color(255, 255, 255));
		lblIngredientes.setBackground(new Color(255, 255, 255));
		lblIngredientes.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblIngredientes.setBounds(901, 145, 155, 23);
		contentPane.add(lblIngredientes);
		
		JLabel label_1 = new JLabel("00 :");
		label_1.setForeground(new Color(255, 255, 255));
		label_1.setFont(new Font("Tahoma", Font.BOLD, 22));
		label_1.setBounds(78, 636, 89, 41);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("00 :");
		label_2.setForeground(new Color(255, 255, 255));
		label_2.setFont(new Font("Tahoma", Font.BOLD, 22));
		label_2.setBounds(146, 636, 89, 41);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("00 :");
		label_3.setForeground(new Color(255, 255, 255));
		label_3.setFont(new Font("Tahoma", Font.BOLD, 22));
		label_3.setBounds(211, 636, 89, 41);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("00");
		label_4.setForeground(new Color(255, 255, 255));
		label_4.setFont(new Font("Tahoma", Font.BOLD, 17));
		label_4.setBounds(266, 638, 89, 41);
		contentPane.add(label_4);
		
		JButton btnComear = new JButton("COME\u00C7AR");
		btnComear.setBackground(new Color(192, 192, 192));
		btnComear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				state = true;
				
				Thread t = new Thread() {
					
					public void run() {
						
						for(;;) {
							if(state == true) {
								try {
									sleep(1);
									if (milisegundos>1000) {
										milisegundos = 0;
										segundos++;
									}
									if (segundos>60) {
										segundos = 0;
										minutos++;
									}
									if (minutos>60) {
										minutos = 0;
										horas++;
									}
									
									label_4.setText(" "+milisegundos);
									milisegundos++;
									label_3.setText(":  "+segundos);
									label_2.setText(":  "+minutos);
									label_1.setText(" "+horas);
									
									System.out.println(milisegundos);
								} catch (Exception e) {
									// TODO: handle exception
								}
							}
						}
					}
				};
				t.start();
			}
		});
		btnComear.setBounds(310, 636, 113, 37);
		contentPane.add(btnComear);
		
		JButton btnParar = new JButton("PARAR");
		btnParar.setBackground(new Color(192, 192, 192));
		btnParar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			state = false;
			}
		});
		btnParar.setBounds(435, 636, 113, 37);
		contentPane.add(btnParar);
		
		JButton btnReset = new JButton("RESET");
		btnReset.setBackground(new Color(192, 192, 192));
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			state = false;
			
			milisegundos = 0;
			segundos = 0;
			minutos = 0;
			horas = 0;
			
			

			label_4.setText("00 ");
			label_3.setText(": 00  ");
			label_2.setText(": 00");
			label_1.setText(" 00");
			
			
			}
		});
		btnReset.setBounds(558, 636, 113, 37);
		contentPane.add(btnReset);
		
			
		
		
	}
}
