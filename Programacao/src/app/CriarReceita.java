package app;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bd.Receitas;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

public class CriarReceita extends JFrame {

	private JPanel contentPane;
	private JTextField nomeReceita;
	private JTextField doses;
	private JTextField tempoP;
	private JTextField tempoC;
	private JTextField tipo;
	private JTextField foto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CriarReceita frame = new CriarReceita();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CriarReceita() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(CriarReceita.class.getResource("/imagens/logo3.png")));
		setTitle("O MESTRE DAS RECEITAS");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 935, 615);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(128, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		this.setLocationRelativeTo(null);
		
		JLabel lblNomeDaReceita = new JLabel("NOME DA RECEITA:");
		lblNomeDaReceita.setForeground(new Color(255, 255, 255));
		lblNomeDaReceita.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNomeDaReceita.setBounds(60, 60, 188, 42);
		contentPane.add(lblNomeDaReceita);
		
		JLabel lblDoses = new JLabel("DOSES:");
		lblDoses.setForeground(new Color(255, 255, 255));
		lblDoses.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblDoses.setBounds(60, 113, 188, 42);
		contentPane.add(lblDoses);
		
		JLabel lblTempoDePreparao = new JLabel("TEMPO DE PREPARA\u00C7\u00C3O");
		lblTempoDePreparao.setForeground(new Color(255, 255, 255));
		lblTempoDePreparao.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblTempoDePreparao.setBounds(60, 166, 188, 42);
		contentPane.add(lblTempoDePreparao);
		
		JLabel lblTempodeConfeo = new JLabel("TEMPO DE CONFE\u00C7\u00C3O:");
		lblTempodeConfeo.setForeground(new Color(255, 255, 255));
		lblTempodeConfeo.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblTempodeConfeo.setBounds(60, 219, 188, 42);
		contentPane.add(lblTempodeConfeo);
		
		nomeReceita = new JTextField();
		nomeReceita.setBackground(new Color(128, 128, 128));
		nomeReceita.setBounds(258, 74, 148, 20);
		contentPane.add(nomeReceita);
		nomeReceita.setColumns(10);
		
		doses = new JTextField();
		doses.setBackground(new Color(128, 128, 128));
		doses.setColumns(10);
		doses.setBounds(258, 127, 148, 20);
		contentPane.add(doses);
		
		tempoP = new JTextField();
		tempoP.setBackground(new Color(128, 128, 128));
		tempoP.setColumns(10);
		tempoP.setBounds(258, 180, 148, 20);
		contentPane.add(tempoP);
		
		tempoC = new JTextField();
		tempoC.setBackground(new Color(128, 128, 128));
		tempoC.setColumns(10);
		tempoC.setBounds(258, 233, 148, 20);
		contentPane.add(tempoC);
		
		tipo = new JTextField();
		tipo.setColumns(10);
		tipo.setBackground(Color.GRAY);
		tipo.setBounds(258, 286, 148, 20);
		contentPane.add(tipo);
		
		JLabel lblTipo = new JLabel("TIPO:");
		lblTipo.setForeground(new Color(255, 255, 255));
		lblTipo.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblTipo.setBounds(60, 272, 188, 42);
		contentPane.add(lblTipo);
		
		JLabel lblDescrio = new JLabel("DESCRI\u00C7\u00C3O:");
		lblDescrio.setForeground(new Color(255, 255, 255));
		lblDescrio.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblDescrio.setBounds(60, 325, 188, 42);
		contentPane.add(lblDescrio); 
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(258, 325, 282, 175);
		contentPane.add(scrollPane);
		
		JTextPane descricao = new JTextPane();
		descricao.setBackground(new Color(128, 128, 128));
		descricao.setToolTipText("");
		scrollPane.setViewportView(descricao);
		
		JButton btnGuardar = new JButton("GUARDAR");
		btnGuardar.setBackground(new Color(192, 192, 192));
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new Receitas().addReceita(nomeReceita.getText(), Integer.parseInt(doses.getText()), Double.parseDouble(tempoP.getText()), Double.parseDouble(tempoC.getText()), tipo.getText(), descricao.getText(), foto.getText());
				new Janela().setVisible(true);
				setVisible(false);
			}
		});
		btnGuardar.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnGuardar.setBounds(632, 469, 127, 42);
		contentPane.add(btnGuardar);
		
		JButton btnCancelar = new JButton("CANCELAR");
		btnCancelar.setBackground(new Color(192, 192, 192));
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				
				new Janela().setVisible(true);
				setVisible(false);
			}
		});
		btnCancelar.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnCancelar.setBounds(769, 469, 126, 42);
		contentPane.add(btnCancelar);
		
		JLabel lblImagem = new JLabel("IMAGEM: ");
		lblImagem.setForeground(Color.WHITE);
		lblImagem.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblImagem.setBounds(529, 60, 188, 42);
		contentPane.add(lblImagem);
		
		foto = new JTextField();
		foto.setColumns(10);
		foto.setBackground(Color.GRAY);
		foto.setBounds(635, 74, 148, 20);
		contentPane.add(foto);
		
		JButton btnUpload = new JButton("UPLOAD");
		btnUpload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				JFileChooser chooser = new JFileChooser();
				chooser.showOpenDialog(null);
				File f = chooser.getSelectedFile();
				String filename = f.getAbsolutePath();
				foto.setText(filename);
			}
		});
		btnUpload.setBackground(Color.LIGHT_GRAY);
		btnUpload.setBounds(793, 73, 89, 23);
		contentPane.add(btnUpload);
		
	}
}
