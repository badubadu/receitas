package app;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bd.Utilizador;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.border.LineBorder;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField utilizador;
	private JPasswordField password;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	} 

	/**
	 * Create the frame.
	 */
	public Login() {
		setTitle("O MESTRE DAS RECEITAS");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Login.class.getResource("/imagens/logo3.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(128, 0, 0));
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		this.setLocationRelativeTo(null);
		
		utilizador = new JTextField();
		utilizador.setBackground(new Color(255, 248, 220));
		utilizador.setBounds(125, 75, 202, 20);
		contentPane.add(utilizador);
		utilizador.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Utilizador:");
		lblNewLabel.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 14));
		lblNewLabel.setBounds(125, 50, 202, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 14));
		lblPassword.setBounds(125, 112, 202, 14);
		contentPane.add(lblPassword);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setBackground(new Color(192, 192, 192));
		btnEntrar.addActionListener(new ActionListener() {
			
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {			
				

				Utilizador conta = new Utilizador();
				
				if(conta.verificaLogin(utilizador.getText(), password.getText())) {
					new Janela().setVisible(true);
					setVisible(false);
				
				}else
					JOptionPane.showMessageDialog(null, "A password est� errada!!!");
			}
		});
		btnEntrar.setBounds(129, 182, 89, 23);
		contentPane.add(btnEntrar);
		
		JButton btnReset = new JButton("Reset");
		btnReset.setBackground(new Color(192, 192, 192));
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				utilizador.setText("");
				password.setText("");
			}
		});
		btnReset.setBounds(238, 182, 89, 23);
		contentPane.add(btnReset);
		
		password = new JPasswordField();
		password.setBackground(new Color(255, 248, 220));
		password.setBounds(125, 137, 202, 20);
		contentPane.add(password);
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		panel.setBackground(new Color(128, 128, 128));
		panel.setBounds(0, 0, 434, 42);
		contentPane.add(panel);
		
		JLabel lblOMestreDas = new JLabel("O MESTRE DAS RECEITAS");
		
		lblOMestreDas.setForeground(new Color(0, 0, 0));
		lblOMestreDas.setFont(new Font("Tahoma", Font.BOLD, 21));
		panel.add(lblOMestreDas);
		
	}
}
