package bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Ingredientes {
	private int ingredientes;
	private String nomeIngredientes;

	private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:test.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

	public int verId(String selecionado) {

		String sql = "SELECT idReceitas FROM Receitas WHERE nome = \""+selecionado +"\"";
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while(rs.next()) {
                ingredientes = rs.getInt("idReceitas");
               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		return ingredientes;
    }
	
	public String verIngredientes(int ingredientes) {

		String sql = "SELECT nome FROM DetalhesReceita WHERE idReceita = "+ingredientes;
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while(rs.next()) {
                nomeIngredientes = rs.getString("nome");
               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		return nomeIngredientes;
    }
}
