package bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Receitas {
	private int id;
	private String nome;
	private String descricao;
	private int dose;
	private String tempoP;
	private String tempoC;
	private String foto;
	private String tipo;
	
	
	private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:test.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
public void addReceita(String nomeReceita) {
		
		String sql = "INSERT INTO Receitas (nome,doses,tempoP,tempoC,tipo,descricao,foto) VALUES (\""+nomeReceita+"\",'','','','','',NULL)";
		
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
        	
        	 
        	 
           
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

public void addReceita(String nomeReceita,int doses,double tempoP, double tempoC, String tipo, String descricao, String foto) {
	
	String sql = "INSERT INTO Receitas (nome,doses,tempoP,tempoC,tipo,descricao,foto) VALUES (\""+nomeReceita+"\","+doses+","+tempoP+","+tempoC+",\""+tipo+"\",\""+descricao+"\",\""+foto+"\")";
	
    try (Connection conn = this.connect();
         Statement stmt  = conn.createStatement();
         ResultSet rs    = stmt.executeQuery(sql)){
    	
    	 
       
    } catch (SQLException e) {
        System.out.println(e.getMessage());
    }
}

public void apagarReceita(String selecionado) {
	
	String sql = "DELETE FROM Receitas WHERE nome = \""+selecionado+"\"";
	
    try (Connection conn = this.connect();
         Statement stmt  = conn.createStatement();
         ResultSet rs    = stmt.executeQuery(sql)){
    	
    	 
    	 
       
    } catch (SQLException e) {
        System.out.println(e.getMessage());
    }
}

	public void listaReceitas() {
		
		String sql = "SELECT nome FROM Receitas";
		
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
        	
        	 
        	 // loop through the result set
            while(rs.next()) {
        
            	System.out.println(rs.getString("nome"));
            	
				
            	
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
	
	public String verDescricao(String selecionado) {

		String sql = "SELECT descricao FROM Receitas WHERE nome = \""+selecionado +"\"";
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            if(rs.next()) {
                descricao = rs.getString("descricao");
               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		return descricao;
    }
	
	public int dose(String selecionado) {

		String sql = "SELECT * FROM Receitas WHERE nome = \""+selecionado +"\"";
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            if(rs.next()) {
                dose = rs.getInt("doses");
               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		return dose;
    }
	
	public String tempoP(String selecionado) {

		String sql = "SELECT * FROM Receitas WHERE nome = \""+selecionado +"\"";
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            if(rs.next()) {
                tempoP = rs.getString("tempoP");
               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		return tempoP;
    }

	public String tempoC(String selecionado) {

		String sql = "SELECT * FROM Receitas WHERE nome = \""+selecionado +"\"";
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            if(rs.next()) {
                tempoC = rs.getString("tempoC");
               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		return tempoC;
    }
	
	public String tipo(String selecionado) {

		String sql = "SELECT * FROM Receitas WHERE nome = \""+selecionado +"\"";
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            if(rs.next()) {
                tipo = rs.getString("tipo");
               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		return tipo;
    }
	
	public String foto(String selecionado) {

		String sql = "SELECT * FROM Receitas WHERE nome = \""+selecionado +"\"";
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            if(rs.next()) {
                foto = rs.getString("foto");
               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		return foto;
    }
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
