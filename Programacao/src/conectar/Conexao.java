package conectar;

import java.sql.*;

public class Conexao {

	static final String JDBC_DRIVER = "org.sqlite.JDBC";
	static final String DB_URL = "jdbc:sqlite:test.db";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER);

			System.out.println("A conectar � BASE DE DADOS...");
			conn = DriverManager.getConnection(DB_URL);

			System.out.println("Quering database...");

			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT FROM ola");

			while(rs.next()) {
				System.out.println(rs.getString(" "));
			}
			System.out.println("Sucesso!!!");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					 conn.close();
					 }catch(SQLException se){
					 se.printStackTrace();
					 }//end finally try
					 }//end try
					 System.out.println("Goodbye!");
					 }//end main
					 }//end JDBCExample
			
		
		

